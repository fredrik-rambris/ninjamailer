package com.rambris.ninjamailer.controller;

import com.rambris.ninjamailer.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/preview")
@RequiredArgsConstructor
public class PreviewController {

    private final TemplateService templateService;


    @PostMapping(path = "/{templateName:^[\\p{Alnum}_-]+}.html", produces = MediaType.TEXT_HTML_VALUE)
    public String html(@PathVariable String templateName, @RequestBody Map<String, Object> vars) throws IOException {
        return templateService.getHtml(templateName.toLowerCase(), vars);
    }

    @PostMapping(path = "/{templateName:^[\\p{Alnum}_-]+}.txt", produces = MediaType.TEXT_PLAIN_VALUE)
    public String text(@PathVariable String templateName, @RequestBody Map<String, Object> vars) throws IOException {
        return templateService.getText(templateName.toLowerCase(), vars);
    }
}

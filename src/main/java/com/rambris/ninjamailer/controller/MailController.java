package com.rambris.ninjamailer.controller;

import com.rambris.ninjamailer.model.SendmailRequest;
import com.rambris.ninjamailer.service.SendmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import java.io.IOException;


@RestController
@RequestMapping(value = "/mail", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class MailController {
    private final SendmailService sendmailService;


    @PostMapping("send")
    private ResponseEntity send(@RequestBody @Validated SendmailRequest req) throws IOException, MessagingException {
        sendmailService.sendMail(req);
        return ResponseEntity.ok("{}");
    }
}

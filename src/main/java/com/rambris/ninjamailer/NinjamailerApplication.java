package com.rambris.ninjamailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NinjamailerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NinjamailerApplication.class, args);
    }

}

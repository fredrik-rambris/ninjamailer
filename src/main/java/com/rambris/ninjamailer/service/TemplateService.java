package com.rambris.ninjamailer.service;

import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.JinjavaConfig;
import com.hubspot.jinjava.loader.ResourceLocator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class TemplateService {
    private final ResourceLocator resourceLocator;

    public TemplateService(@Value("${template.path}") String templatePath) throws FileNotFoundException {
        resourceLocator = new ProtectedFileLocator(new File(templatePath));

        var config = JinjavaConfig.newBuilder()
                .build();

        this.jinja = new Jinjava(config);

        jinja.setResourceLocator(resourceLocator);
    }

    private final Jinjava jinja;

    public String getHtml(String templateName, Map<String, Object> vars) throws IOException {
        var jtemplate = resourceLocator.getString(templateName + ".html.j2", StandardCharsets.UTF_8, null);
        return jinja.render(jtemplate, vars);
    }

    public String getText(String templateName, Map<String, Object> vars) throws IOException {
        var jtemplate = resourceLocator.getString(templateName + ".txt.j2", StandardCharsets.UTF_8, null);
        return jinja.render(jtemplate, vars);
    }

}

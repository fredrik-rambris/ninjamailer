package com.rambris.ninjamailer.service;

import com.rambris.ninjamailer.model.SendmailRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class SendmailService {
    private final JavaMailSender emailSender;
    private final TemplateService templateService;

    public void sendMail(SendmailRequest req) throws IOException, MessagingException {
        var message = emailSender.createMimeMessage();
        var helper = new MimeMessageHelper(message, true);

        helper.setFrom(req.getFrom());
        helper.setTo(req.getTo());
        helper.setSubject(req.getSubject());

        helper.setText(
                templateService.getText(req.getTemplate().toLowerCase(), req.getVars()),
                templateService.getHtml(req.getTemplate().toLowerCase(), req.getVars())
        );

        emailSender.send(message);
    }
}

package com.rambris.ninjamailer.service;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.loader.ResourceLocator;
import com.hubspot.jinjava.loader.ResourceNotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class ProtectedFileLocator implements ResourceLocator {
    private File baseDir;

    public ProtectedFileLocator(File baseDir) throws FileNotFoundException {
        if (!baseDir.exists()) {
            throw new FileNotFoundException(String.format("Specified baseDir [%s] does not exist", baseDir.getAbsolutePath()));
        }
        this.baseDir = baseDir;
    }

    private File resolveFileName(String name) {
        File f = new File(baseDir, name);
        if (!f.getAbsolutePath().startsWith(baseDir.getAbsolutePath())) {
            return null;
        } else {
            return f;
        }
    }

    @Override
    public String getString(String name, Charset charset, JinjavaInterpreter jinjavaInterpreter) throws IOException {
        File file = resolveFileName(name);

        if (file == null || !file.exists() || !file.isFile()) {
            throw new ResourceNotFoundException("Couldn't find resource: " + file);
        }

        return Files.readString(file.toPath(), charset);
    }


}

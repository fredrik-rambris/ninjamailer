package com.rambris.ninjamailer.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@Data
public class SendmailRequest {
    @Email
    private String from;

    @NotBlank
    private String subject;

    @Email
    @NotBlank
    private String to;

    private String template="default";

    private Map<String, Object> vars=Map.of();
}
